#include "newuserdialog.h"
#include "ui_newuserdialog.h"
#include <validationuserdialog.h>
#include <homedialog.h>
#include <QDebug>
#include <fstream>
#include <QString>

ofstream file;

NewUserDialog::NewUserDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::NewUserDialog)
{
    ui->setupUi(this);
}

NewUserDialog::~NewUserDialog()
{
    delete ui;
}


QString NewUserDialog::getUserName()
{
    return usern;
}

QString NewUserDialog::getPassword()
{
    return pass;
}

bool NewUserDialog::getFlag()
{
    return flag;
}

void NewUserDialog::on_btnRegresar_3_clicked()
{
    this->hide();
    validationuserDialog *valiDialog = new validationuserDialog();
    valiDialog->setModal(true);
    valiDialog->setWindowFlags(Qt::Window | Qt::FramelessWindowHint);
    valiDialog->show();
}

void NewUserDialog::on_pushButton_clicked()
{
    QString username = ui->leUsername_3->text();
    QString passWord = ui->lePassword_3->text();
    login = new LoginDialog();
    login->setVal(&username, &passWord);
    QString file = "users.txt";
    QFile outputFile(file);
    outputFile.open(QIODevice::WriteOnly);

    outputFile.close();
    for(int i=0;i<login->getList().size();i++){
        Usuario *userTemp = login->getList().at(i);
        if(!outputFile.isOpen())
        {

        }
        QTextStream outStream(&outputFile);
        outStream << userTemp->getUsername() << " : " << userTemp->getPassword();
        outputFile.close();
    }

    this->hide();
    login->setModal(true);
    login->setWindowFlags(Qt::Window | Qt::FramelessWindowHint);
    login->show();

}

void NewUserDialog::updateUserlist(){
    ofstream file;
    for (int i=0; i<login->getList().size(); i++){
        Usuario *userTemp = login->getList().at(i);
    }



}
