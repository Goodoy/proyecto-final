#include "validationuserdialog.h"
#include "ui_validationuserdialog.h"
#include <homedialog.h>

validationuserDialog::validationuserDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::validationuserDialog)
{
    ui->setupUi(this);
}

validationuserDialog::~validationuserDialog()
{
    delete ui;
}

void validationuserDialog::on_pushButton_2_clicked()
{
    this->hide();
    newuser = new NewUserDialog();
    newuser->setModal(true);
    newuser->setWindowFlags(Qt::Window | Qt::FramelessWindowHint);
    newuser->exec();
}

void validationuserDialog::on_btnRegresar_4_clicked()
{
    this->hide();
    login = new LoginDialog();
    login->setModal(true);
    login->setWindowFlags(Qt::Window | Qt::FramelessWindowHint);
    login->exec();
}



void validationuserDialog::on_pushButton_clicked()
{
    this->hide();
    HomeDialog *homeDialog = new HomeDialog();
    homeDialog->setModal(true);
    homeDialog->setWindowFlags(Qt::Window | Qt::FramelessWindowHint);
    homeDialog->show();
}
