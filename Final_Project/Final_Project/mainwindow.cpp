#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "homedialog.h"
#include <QDebug>
#include <QGraphicsView>
#include <QDesktopWidget>

int goals1 = 0;
int goals2 = 0;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)

{    
    ui->setupUi(this);
    centerAndResize();
    x_limit = 1000;
    y_limit = 500;
    dt = 0.1;                            //Asigna el lapso de tiempo
    timer = new QTimer(this);                 //crea el timer
    scene = new QGraphicsScene(this);         //crea la scene
    scene->setSceneRect(0,0,x_limit,y_limit);     //asigna el rectangulo que encierra la scene, determinado por h_limit y v_limit
    ui->graphicsView->setScene(scene);
    ui->centralWidget->adjustSize();
    scene->addRect(scene->sceneRect());
    ball = new BallGraf(500, 250, 15);          //Creacion de la pelota
    ball->actualizar(dt, y_limit);
    scene->addItem(ball);                       //Aparicion en escena
    ui->label->hide();
    ui->label_2->hide();
    ui->label_3->hide();
    ui->label_4->hide();
    player1 = new Grafica1;                     //Creacion del Jugador 1
    player1->setPos(170,402);
    player1->setScale(0.6);
    scene->addItem(player1);                    //Aparicion en escena
    player2 = new Grafica2;                     //Creacion del Jugador 2
    player2->setPos(700,402);
    player2->setScale(0.6);
    scene->addItem(player2);                    //Aparicion en escena
    goal1 = new GoalGraf1(500,480,70,100);      //Creacion de la cancha
    goal1->setPos(65,405);
    scene->addItem(goal1);                      //Aparicion en escena
    goal2 = new GoalGraf2(500,480,70,100);      //Cancha numero 2
    goal2->setPos(935,405);
    scene->addItem(goal2);                      //Aparicion en escena
    l2=new QGraphicsLineItem(0, 500, 0, 0);
    scene->addItem(l2);
    timer->stop();                              //para el timer
    connect(timer,SIGNAL(timeout()),this,SLOT(actualizar()));
    if(flag==true){
        timer->start(130*dt);                   //Timer para el juego y la correcta funcion de las fisicas
    }

    ui->lcdNumber_2->display(goals1);
    ui->lcdNumber_3->display(goals2);
}

MainWindow::~MainWindow()           //Destructor de la escena
{
    delete timer;
    delete scene;
    delete ui;
}

void MainWindow::centerAndResize() {        //Establece la geometria de la escena
     setGeometry(
        QStyle::alignedRect(
            Qt::LeftToRight,
            Qt::AlignCenter,
            this->size(),
            qApp->desktop()->availableGeometry()
        )
    );
}

void MainWindow::actualizar()  //Función de la escena
{
    bordercol(ball->getEsf());

    ball->actualizar(dt,y_limit);

    if (saltando1){
        player1->setPos(player1->x(), player1->y()-cont1);
        player1->getP()->setPy(player1->y()-cont1);
        if(player1->y()<=340){
            cont1 = cont1*-1;
        }
        if(player1->y() == 402){
            saltando1 = false;          //Animacion del salto jugador 1
            cont1 = 3.8;
        }
    }
    if (saltando2){
        player2->setPos(player2->x(), player2->y()-cont2);
        player2->getP()->setPy(player2->y()-cont1);
        if(player2->y()<=340){
            cont2 = cont2*-1;
        }
        if(player2->y() == 402){
            saltando2 = false;      //Salto del jugador 2
            cont2 = 3.8;
        }
    }
    if(mover1){
        player1->setPos(player1->x()+2, player1->y());      //Movimiento a la derecha Jugador 1
    }
    if(mover2){
        player1->setPos(player1->x()-2, player1->y());      //Movimiento a la izquierda Jugador 1
    }

    if(mover3){
        player2->setPos(player2->x()-2, player2->y());      //Movimiento a la izquierda Jugador 2
    }

    if(mover4){
        player2->setPos(player2->x()+2, player2->y());      //Movimiento a la derecha Jugador 2
    }

    if(player1->x() <= 0){  //Bloqueo a la izquierda
        mover2 = false;
    }

    if(kicking1){           //Animacion de la pateada Jugador 1
            if (flag1==0 && c1==0)
            {
                player1->setPixmap(QPixmap(":/Animacion2.png"));
                flag1=1;
            }
            else if (flag1==1 && c1==2)
            {
                player1->setPixmap(QPixmap(":/Animacion3.png"));
                flag1=0;
            }
            else if (flag1==0 && c1==4)                                 //Secuencia de imagenes Jugador 1
            {
                player1->setPixmap(QPixmap(":/Animacion4.png"));
                flag1=1;
            }
            else if (flag1==1 && c1==6)
            {
                player1->setPixmap(QPixmap(":/Animacion5.png"));
                flag1=0;
            }


            if (c1>150)c1=0;
            c1++;
    }

    if(kicking2){           //Animacion de la pateada Jugador 2
            if (flag2==0 && c2==0)
            {
                player2->setPixmap(QPixmap(":/Animacion2.1.png"));
                flag2=1;
            }
            else if (flag2==1 && c2==2)
            {
                player2->setPixmap(QPixmap(":/Animacion3.1.png"));
                flag2=0;
            }
            else if (flag2==0 && c2==4)                                 //Secuencia de imagenes
            {
                player2->setPixmap(QPixmap(":/Animacion4.1.png"));
                flag2=1;
            }
            else if (flag2==1 && c2==6)
            {
                player2->setPixmap(QPixmap(":/Animacion5.1.png"));
                flag1=0;
            }


            if (c2>150)c2=0;
            c2++;
    }


    ui->lcdNumber->display(time/100);

    time= time-1;
    if(time<99){    //Temporizador de juego
        timer->stop();
        flag = false;
        if(goals1>goals2){
            ui->label_3->show(); //Ganador jugador 2
        }
        else{
            if(goals1<goals2){
                ui->label_2->show(); //Ganador jugador 1
            }
            else{
                if(goals1==goals2){
                    ui->label_4->show();  //Empate
                }
            }
        }
    }
}


void MainWindow::bordercol(Ball *esf)
{

    if(esf->getX() <= (esf->getR()+2) and esf->getVx() < 0){ // left collision, limite de la escena
        esf->setVel(esf->get_E()*esf->getVx() , esf->getVy());
    }

    if(esf->getX() >= (x_limit - esf->getR()-2) and esf->getVx() > 0){// Right collision, limite de la escena
        esf->setVel(esf->get_E()*esf->getVx() , esf->getVy());
    }

    if(esf->getY() >= y_limit -(esf->getR()) && esf->getVy()>0){ //Up collision, limite de la escena
        esf->setVel(esf->getVx(),-1*esf->getVy());
    }

    if(esf->getY() <= esf->getR() && esf->getVy()<0){ //Down collision, limite de la escena
        esf->setVel(esf->getVx(), esf->get_E()*esf->getVy());
    }

    if(esf->getY() <= esf->getR()){// Restitution collision
        esf->setY(esf->getR());
    }

    if(esf->getY() <= player1->y()-300 and esf->getY()>= player1->y()-400 && esf->getX() >= (player1->x()+10)-esf->getR()-2 && esf->getX() <= (player1->x()+30)-esf->getR()-2 and esf->getVx()>0){
        esf->setVel(esf->get_E() * esf->getVx(), esf->getVy()); //Back Collision
    }

    if(esf->getY() <= player1->y()-300 && esf->getX() <= (player1->x()+100)-esf->getR()-2 && esf->getX() >= (player1->x()+80)-esf->getR()-2 and esf->getVx()<0){
        esf->setVel(esf->get_E() * esf->getVx(), esf->getVy()); //Front collision
    }

    if(esf->getY()<=player1->y()-300 && esf->getX()<= player1->x()+90 && esf->getX()>= player1->x()+10){
        esf->setVel(esf->getVx(), esf->get_E()*esf->getVy()); //Down Collision
    }

    if(esf->getY() <= player2->y()-300 && esf->getX() >= (player2->x()+50)-esf->getR()-2 && esf->getX() <= (player2->x()+70)-esf->getR()-2 and esf->getVx()>0){
        esf->setVel(esf->get_E() * esf->getVx(), esf->getVy()); //Front Collision
    }

    if(esf->getY() <= player2->y()-300 && esf->getX() <= (player2->x()+140)-esf->getR()-2 && esf->getX() >= (player2->x()+90)-esf->getR()-2 and esf->getVx()<0){
        esf->setVel(esf->get_E() * esf->getVx(), esf->getVy()); //Back Collision
    }

    if(esf->getY()<=player2->y()-300 && esf->getX()<= player2->x()+140 && esf->getX()>= player2->x()+50){
        esf->setVel(esf->getVx(), esf->get_E()*esf->getVy()); //Down Collision
    }

    if(esf->getX()>=(player1->x()+70)-esf->getR()-2  && kicking1 && esf->getX()<=(player1->x()+90)-esf->getR()-2){
        esf->setVel(90 , 90);  //Colision con la pateada
    }

    if(esf->getX()>=(player2->x()+50)-esf->getR()-2  && kicking2 && esf->getX()<=(player2->x()+70)-esf->getR()-2){
        esf->setVel(-90 , -90); //Colision con la patada
    }

    if(esf->getX()<= 80 and esf->getY()<=100){ //Si la esfera ingresa en este espacio
        ui->label->show();
        ball->getEsf()->setX(500);
        ball->getEsf()->setY(0);

        goals1++;

        goal=50;
        left=1;
        ui->lcdNumber_2->display(goals1);  //Gol de Jugador 1
    }

    if(esf->getX()>= 920 and esf->getY()<=100){  //Si la esfera ingresa en este espacio
        ui->label->show();
        ball->getEsf()->setX(500);
        ball->getEsf()->setY(0);

        goals2+=1;

        goal=50;
        left=2;
        ui->lcdNumber_3->display(goals2); //Gol de Jugador 1
    }


    if(goal==0){ //Reinicio de la escena despues del gol

        player1->setPos(170,402); //Posiciones iniciales de todos los cuerpos
        player2->setPos(700,402);
        ball->getEsf()->setX(500);
        ball->getEsf()->setY(250);
        ui->label->hide();
        if(left == 2){
            ball->getEsf()->setVel(-10, 0);
        }
    }
    goal--;
}


void MainWindow::keyPressEvent(QKeyEvent* event){  //Captacion de las entradas

    if(event->key()==Qt::Key_W)  //Tecla a recibir
    {
        saltando1=true;   //Función
    }

    if(event->key()==Qt::Key_I)  //Tecla a recibir
    {
        saltando2=true;   //Función
    }

    if(event->key()==Qt::Key_D)  //Tecla a recibir
    {
        mover1=true;   //Función
    }

    if(event->key()==Qt::Key_A)  //Tecla a recibir
    {
        mover2=true;   //Función
    }

    if(event->key()==Qt::Key_J)  //Tecla a recibir
    {
        mover3=true;   //Función
    }

    if(event->key()==Qt::Key_L)  //Tecla a recibir
    {
        mover4=true;   //Función
    }

    if(event->key()==Qt::Key_Space)  //Tecla a recibir
    {
        kicking1 = true;   //Función
    }
    if(event->key()==Qt::Key_P)  //Tecla a recibir
    {
        kicking2 = true;   //Función
    }

    if(event->key()==Qt::Key_R){
        timer->start(150*dt);
    }

}

void MainWindow::keyReleaseEvent(QKeyEvent* event){   //Captacion de las entradas

    if(event->key()==Qt::Key_D)  //Tecla a recibir
    {
        mover1=false;   //Función
    }


    if(event->key()==Qt::Key_A)  //Tecla a recibir
    {
        mover2=false;   //Función
    }

    if(event->key()==Qt::Key_J)  //Tecla a recibir
    {
        mover3=false;   //Función
    }

    if(event->key()==Qt::Key_L)  //Tecla a recibir
    {
        mover4=false;   //Función
    }

    if(event->key()==Qt::Key_Space)  //Tecla a recibir
    {
        c1=0;
        flag1=0;                   //Función
        kicking1 = false;
        player1->setPixmap(QPixmap(":/player1.png"));

    }

    if(event->key()==Qt::Key_P)  //Tecla a recibir
    {
        c2=0;
        flag2=0;                   //Función
        kicking2 = false;
        player2->setPixmap(QPixmap(":/player2.png"));
    }
}

void MainWindow::on_btnRegresar_clicked()
{
    this->hide();
    HomeDialog *homeDialog = new HomeDialog () ;
    homeDialog->setModal(true);
    homeDialog->setWindowFlags(Qt::Window | Qt::FramelessWindowHint);
    homeDialog->show();    
}

