#include "ball.h"
#include <QDebug>
#define PI 3.14159265358979323846264338
#define G 10

Ball::Ball(double px, double py, double r):px(px), py(py), radio(r), ax(0), ay(0)
{
    double ro = 0.01;
    double Cf = 0.005;
    vx = 10;
    vy = 10;
    masa = 450;
    E = -0.75;
    K = ro*Cf*PI/2.0;
}

Ball::~Ball(){
}

void Ball::setVel(double x, double y)
{
    vx= x;
    vy= y;
}



void Ball::actualizar(double dt)
{
    double v = sqrt(pow((vx),2)+pow((vy),2));
    double theta = atan2(vy,vx);
    ax = -1*(K*(v*v)*(radio*radio)*cos(theta))/masa;
    ay = (-1*(K*(v*v)*(radio*radio)*sin(theta))/masa) - G;
    px += vx*dt + (ax/2)*pow(dt,2);
    py += vy*dt + (ay/2)*pow(dt,2);
    vx += ax*dt;
    vy += ay*dt;
}

double Ball::getX() const
{
    return px;
}

double Ball::getY() const
{
    return py;
}

double Ball::setY(double y)
{
    py = y;
}

double Ball::setX(double x)
{
    px = x;
}

double Ball::getR() const
{
    return radio;
}

double Ball::getVx() const
{
    return vx;
}

double Ball::getVy() const
{
    return vy;
}

double Ball::get_E() const
{
    return E;
}



