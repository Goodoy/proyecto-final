#ifndef LOGINDIALOG_H
#define LOGINDIALOG_H

#include <QDialog>
#include "usuario.h"
#include <QMessageBox>
#include <mainwindow.h>


namespace Ui {
class LoginDialog;
}

class LoginDialog : public QDialog
{
    Q_OBJECT

public:
    explicit LoginDialog(QWidget *parent = nullptr);
    ~LoginDialog();
    bool validarUsuario(QString username, QString password);
    void createUser(QString &, QString &);
    QList<Usuario*> getList();
    void setVal(QString *, QString *);
    void actualizarListUser();

private slots:

    void on_btnLoginUser1_clicked();

    void on_btnLoginUser_2_clicked();

    void on_btnComenzar_clicked();

    void on_btnRegresar_clicked();

private:
    Ui::LoginDialog *ui;
    QList<Usuario*> listUser;
    MainWindow *gameWindow;
    QString username;
    QString pass;

    bool stateUser1,stateUser2;



};

#endif // LOGINDIALOG_H
