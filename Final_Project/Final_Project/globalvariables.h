#ifndef GLOBALVARIABLES_H
#define GLOBALVARIABLES_H
#include "usuario.h"


class GlobalVariables
{
public:
    GlobalVariables(QList<Usuario*> listUser);
    QList<Usuario*> getListUser();
    void setListUser(QList<Usuario*> listUser);

private:
    QList<Usuario*> listUser;
};

#endif // GLOBALVARIABLES_H
