#ifndef BALLGRAF_H
#define BALLGRAF_H


#include <QPainter>
#include <qbrush.h>
#include <QGraphicsItem>
#include <QGraphicsScene>
#include "ball.h"

class BallGraf: public QGraphicsItem          //Clase para graficar las esferas
{
public:
    BallGraf(double x, double y,double r);

    ~BallGraf();
    QRectF boundingRect() const;    //necesario definirla, devuelve el rectangulo que encierra el objeto
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget); //define como se pintara el objeto
    void setEscala(double s);
    void actualizar(double dt, double v_lim);
    Ball* getEsf();
private:
    Ball *esf;
    double escala;
};


#endif // BALLGRAF_H
