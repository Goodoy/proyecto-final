#include "logindialog.h"
#include "ui_logindialog.h"
#include <QDebug>
#include <homedialog.h>
#include <fstream>

LoginDialog::LoginDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::LoginDialog)
{
    ui->setupUi(this);    
    stateUser1 = false;
    stateUser2 = false;

    QString username1 = "pepiperez";
    QString pass1 = "12345";
    createUser(username1, pass1);
}

LoginDialog::~LoginDialog()
{
    delete ui;
}

bool LoginDialog::validarUsuario(QString username,QString password)
{

    for(int i=0;i<listUser.size();i++){
        Usuario *userTemp = listUser.at(i);
        qDebug()<<userTemp->getUsername()<<":"<<userTemp->getPassword();
        if(userTemp->getUsername()==username){
            if(userTemp->getPassword() == password){
                return true;
            }
        }
    }
    return false;
}

QList<Usuario*> LoginDialog::getList()
{
    return listUser;
}

void LoginDialog::createUser(QString &username, QString &pass)
{
    Usuario *user = new Usuario();
    user->setUsername(username);
    user->setPassword(pass);
    listUser.append(user);
}



void LoginDialog::on_btnLoginUser1_clicked()
{
    QString username = ui->leUsername->text();
    QString passWord = ui->lePassword->text();
    if(validarUsuario(username, passWord)){
        QMessageBox::information(this,"Login","Usuario correcto");
        stateUser1 = true;
    }else{
        QMessageBox::information(this,"Login","Usuario incorrecto");
    }
}

void LoginDialog::on_btnLoginUser_2_clicked()
{
    QString username = ui->leUsername_2->text();
    QString passWord = ui->lePassword_2->text();
    if(validarUsuario(username,passWord)){
        QMessageBox::information(this,"Login","Usuario correcto");
        stateUser2 = true;
    }else{
        QMessageBox::information(this,"Login","Usuario incorrecto");
    }

}

void LoginDialog::on_btnComenzar_clicked()
{
    if(stateUser1&&stateUser2){
        this->hide();
        gameWindow = new MainWindow();
        gameWindow->setWindowFlags(Qt::Window | Qt::FramelessWindowHint);
        gameWindow->show();
    }else{
        QMessageBox::warning(this,"Error","Aún no puede iniciar la partida hasta que los dos usuarios inicien sesion");
    }

}

void LoginDialog::on_btnRegresar_clicked()
{
    this->hide();
    validationuserDialog *homeDialog = new validationuserDialog();
    homeDialog->setModal(true);
    homeDialog->setWindowFlags(Qt::Window | Qt::FramelessWindowHint);
    homeDialog->show();
}

void LoginDialog::setVal(QString *usern, QString *passw){
    username = *usern;
    pass = *passw;
    createUser(username, pass);
}

void LoginDialog::actualizarListUser()
{




}
