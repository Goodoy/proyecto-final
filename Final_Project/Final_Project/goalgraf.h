#ifndef GOALGRAF_H
#define GOALGRAF_H

#include <QGraphicsItem>
#include "goal.h"

class GoalGraf1: public QGraphicsItem          //Clase para graficar las esferas
{
public:
    GoalGraf1(double x, double y,double b, double h);

    ~GoalGraf1();
    QRectF boundingRect() const;    //necesario definirla, devuelve el rectangulo que encierra el objeto
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget); //define como se pintara el objeto
    void setEscala(double s);
    void actualizar(double dt, double v_lim);
    Goal* getGoal();
private:
    Goal *goal1;
    double escala;
};



class GoalGraf2: public QGraphicsItem          //Clase para graficar las esferas
{
public:
    GoalGraf2(double x, double y,double b, double h);

    ~GoalGraf2();
    QRectF boundingRect() const;    //necesario definirla, devuelve el rectangulo que encierra el objeto
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget); //define como se pintara el objeto
    void setEscala(double s);
    void actualizar(double dt, double v_lim);
    Goal* getGoal();
private:
    Goal *goal2;
    double escala;
};

#endif // GOALGRAF_H
