#ifndef USUARIO_H
#define USUARIO_H

#include <QObject>
#include <QString>

class Usuario
{
public:

    Usuario();
    virtual ~Usuario();
    void setUsername(QString username);
    void setPassword(QString password);
    void setName(QString name);
    void setLastName(QString lastName);
    QString getUsername();
    QString getPassword();
    QString getName();
    QString getLastName();

private:
//    QString name;
//    QString lastName;
    QString username;
    QString password;

};



#endif // USUARIO_H
