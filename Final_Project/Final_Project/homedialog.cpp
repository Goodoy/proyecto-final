#include "homedialog.h"
#include "ui_homedialog.h"

HomeDialog::HomeDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::HomeDialog)
{
    ui->setupUi(this);
    mMediaPlayer = new QMediaPlayer(this);

    playlist = new QMediaPlaylist;
    playlist->addMedia(QUrl::fromLocalFile("C:/Users/Estiven C/Documents/Final_Project/song.mp3"));
    playlist->setCurrentIndex(1);
    mMediaPlayer->setPlaylist(playlist);
    mMediaPlayer->setVolume(1000);
    mMediaPlayer->play();
}

HomeDialog::~HomeDialog()
{
    delete ui;
}


void HomeDialog::on_btnStartGame_clicked()
{
    validationDialog = new ValidationDialog();
    validationDialog->setWindowFlags(Qt::Window | Qt::FramelessWindowHint);
    validationDialog->setModal(true);
    validationDialog->show();
    this->hide();
}

void HomeDialog::on_btnInstruction_clicked()
{
    instruccionesDialog = new InstructionsDialog();
    instruccionesDialog->setModal(true);
    instruccionesDialog->show();
    this->hide();
}

void HomeDialog::on_btnExit_clicked()
{
    this->close();

}

QMediaPlayer *HomeDialog::getMediaPlayer() const
{
    return mMediaPlayer;
}

QMediaPlaylist *HomeDialog::getPlaylist() const
{
    return playlist;
}
