#-------------------------------------------------
#
# Project created by QtCreator 2018-11-29T13:47:10
#
#-------------------------------------------------

QT       += core gui widgets
QT       += core gui multimedia
CONFIG += resources_big

TARGET = Final_Project
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

CONFIG += c++11

SOURCES += \
        main.cpp \
        mainwindow.cpp \
    ball.cpp \
    ballgraf.cpp \
    playergraf.cpp \
    goal.cpp \
    goalgraf.cpp \
    player.cpp \
    homedialog.cpp \
    instructionsdialog.cpp \
    validationdialog.cpp \
    logindialog.cpp \
    usuario.cpp \
    newuserdialog.cpp \
    validationuserdialog.cpp

HEADERS += \
        mainwindow.h \
    player.h \
    ball.h \
    ballgraf.h \
    playergraf.h \
    goal.h \
    goalgraf.h \
    homedialog.h \
    instructionsdialog.h \
    validationdialog.h \
    logindialog.h \
    usuario.h \
    newuserdialog.h \
    validationuserdialog.h

FORMS += \
        mainwindow.ui \
    homedialog.ui \
    instructionsdialog.ui \
    validationdialog.ui \
    logindialog.ui \
    newuserdialog.ui \
    validationuserdialog.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES += \
    resource.qrc

DISTFILES += \
    ../proyecto-final/Animations/Character2 Left/Animacion2.png \
    ../proyecto-final/Animations/Character2 Left/Animacion3.png \
    ../proyecto-final/Animations/Character2 Left/Animacion4.png \
    ../proyecto-final/Animations/Character2 Left/Animacion5.png \
    ../proyecto-final/Animations/Character2 Left/Animacion6.png
