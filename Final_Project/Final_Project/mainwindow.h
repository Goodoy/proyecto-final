#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QGraphicsScene>
#include <QGraphicsView>
#include <QTimer>
#include <QFile>
#include <time.h>
#include <QKeyEvent>
#include "ball.h"
#include "ballgraf.h"
#include "player.h"
#include "playergraf.h"
#include "goal.h"
#include "goalgraf.h"
#include <QMediaPlayer>
#include <QMediaPlaylist>




namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
    bool saltando1=false;
    bool saltando2 = false;
    bool mover1 = false;
    bool mover2 = false;
    bool mover3 = false;
    bool mover4 = false;
    bool kicking1 = false;
    bool kicking2 = false;
    int flag1=0, c1=0, flag2=0, c2=0;

    void centerAndResize();
private slots:
    void actualizar();
    void keyPressEvent( QKeyEvent* event );
    void keyReleaseEvent(QKeyEvent *ev);


    void on_btnRegresar_clicked();

private:
    Ui::MainWindow *ui;
    QTimer *timer;              //timer para los intervalos de tiempo entre cada frame
    QGraphicsScene *scene;      //scene que muestra los objetos animados
    double dt;                   //intervalo de tiempo entre frames
    int x_limit;                //longitud en X del mundo
    int y_limit;                //longitud en Y del mundo
    void bordercol(Ball *esf);
    BallGraf *ball;
    Grafica1 *player1;
    Grafica2 *player2;          //Graficos a usar
    GoalGraf1 *goal1;
    GoalGraf2 *goal2;
    double cont1 = 3.8, cont2 = 3.8;
    QGraphicsLineItem* l1;
    QGraphicsLineItem* l2;
    bool flag = true;
    int goal;
    int left = 0;
    int time=3000;
    QMediaPlayer *mMediaPlayer;
    QMediaPlayer *mMediaPlayer1;
    QMediaPlaylist *play;
    QMediaPlaylist *play1;    


};
#endif // MAINWINDOW_H
