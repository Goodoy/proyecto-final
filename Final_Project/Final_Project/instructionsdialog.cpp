#include "instructionsdialog.h"
#include "ui_instructionsdialog.h"

#include "homedialog.h"

InstructionsDialog::InstructionsDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::InstructionsDialog)
{
    ui->setupUi(this);
}

InstructionsDialog::~InstructionsDialog()
{
    delete ui;
}



void InstructionsDialog::on_btnRegresar_clicked()
{
    this->hide();
    HomeDialog *homeDialog = new HomeDialog () ;
    homeDialog->setModal(true);
    homeDialog->setWindowFlags(Qt::Window | Qt::FramelessWindowHint);
    homeDialog->show();
}
