#ifndef HOMEDIALOG_H
#define HOMEDIALOG_H

#include <QDialog>
#include "mainwindow.h"
#include "validationdialog.h"
#include "instructionsdialog.h"
#include <QMediaPlayer>
#include <QMediaPlaylist>


namespace Ui {
class HomeDialog;
}

class HomeDialog : public QDialog
{
    Q_OBJECT

public:
    explicit HomeDialog(QWidget *parent = nullptr);
    ~HomeDialog();


    QMediaPlaylist *getPlaylist() const;

    QMediaPlayer *getMediaPlayer() const;

private slots:
    void on_btnStartGame_clicked();

    void on_btnInstruction_clicked();

    void on_btnExit_clicked();



private:
    Ui::HomeDialog *ui;
    MainWindow *gameWindow;
    ValidationDialog *validationDialog;
    InstructionsDialog *instruccionesDialog;
    QMediaPlayer *mMediaPlayer;
    QMediaPlaylist *playlist;
};

#endif // HOMEDIALOG_H
