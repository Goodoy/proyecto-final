#ifndef BALL_H
#define BALL_H


#include <time.h>
#include <math.h>

class Ball{

private:

    double px;                       //posicion en X
    double py;                       //posicion en Y
    const double radio;                //radio
    double masa;
    double vx;                       //velocidad en X
    double vy;                       //velocidad en Y
    double ax;                       //aceleracion en X
    double ay;                       //aceleracion en Y
    double E;                        //Coeficiente de restitucion
    double K;                        //Coeficiente...

public:
    Ball(double px, double py, double r);
    ~Ball();
    void setVel(double vx, double vy);          //metodo para asignar la velocidad (en X y en Y)
    void actualizar(double dt);              //metodo que cambia los valores de posicion (realiza el movimiento), recibe el lapso de tiempo transcurrido

    double getX() const;                  //metodos para retornar los atributos
    double getY() const;
    double setY(double py);
    double setX(double px);
    double getR() const;
    double getVx() const;
    double getVy() const;
    double get_E() const;
};


#endif // BALL_H
