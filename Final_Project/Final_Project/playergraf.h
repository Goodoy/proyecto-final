#ifndef PLAYERGRAF_H
#define PLAYERGRAF_H

#include <QPainter>
#include <qbrush.h>
#include <QGraphicsItem>
#include <QGraphicsScene>
#include <iostream>
using namespace std;
#include "player.h"



class Grafica1:public QObject,
              public QGraphicsPixmapItem

{
    Q_OBJECT
private:
    Player *p1;
    float escala=1;
    bool flag=1;
    int cont=1;
public:
    Grafica1(QGraphicsItem* player1 = 0);
    void kicking();
    void par(float v_limit);
    Player* getP();
    void posicion(float v_lim);
    ~Grafica1();
    float get_escala();
};


class Grafica2:public QObject,
              public QGraphicsPixmapItem
{
    Q_OBJECT
private:
    Player *p2;
    float escala=1;
    bool flag=1;
    int cont=1;
public:
    Grafica2(QGraphicsItem* player2 = 0);
    void kicking();
    void par(float v_limit);
    Player* getP();
    void posicion(float v_lim);
    ~Grafica2();
    float get_escala();
};


#endif // PLAYERGRAF_H
