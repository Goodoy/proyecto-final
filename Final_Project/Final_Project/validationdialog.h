#ifndef VALIDATIONDIALOG_H
#define VALIDATIONDIALOG_H

#include <QDialog>
#include "mainwindow.h"
#include <QTime>
#include <QMessageBox>
#include "logindialog.h"
#include "validationuserdialog.h"

namespace Ui {
class ValidationDialog;
}

class ValidationDialog : public QDialog
{
    Q_OBJECT

public:
    explicit ValidationDialog(QWidget *parent = nullptr);
    ~ValidationDialog();

    void generarNumerosAleatorios();

private slots:
    void on_btnValidar_clicked();

private:
    Ui::ValidationDialog *ui;
    MainWindow *gameWindow;
    int randNum1,randNum2;
    LoginDialog *loginDialog;
    validationuserDialog *valuser;

};

#endif // VALIDATIONDIALOG_H
