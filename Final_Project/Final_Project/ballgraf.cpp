#include "ballgraf.h"
#include "QDebug"



BallGraf::BallGraf(double x, double y, double r) : escala(1)
{
    esf = new Ball(x,y,r);
}

BallGraf::~BallGraf()
{
    delete esf;
}

QRectF BallGraf::boundingRect() const
{
        return QRectF(-1*escala*esf->getR(),-1*escala*esf->getR(),2*escala*esf->getR(),2*escala*esf->getR());
}

void BallGraf::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
//    painter->setBrush(Qt::black);
//    painter->drawEllipse(boundingRect());    //dibuja una elipse encerrada en un boundingRect*/
    QPixmap pixmap;
    pixmap.load(":/ball.png");
    painter->drawPixmap(boundingRect(),pixmap,pixmap.rect());
}


void BallGraf::setEscala(double s)
{
    escala = s;
}

void BallGraf::actualizar(double dt, double v_lim)
{
    esf->actualizar(dt);
    setPos(esf->getX()*escala,(v_lim-esf->getY())*escala);
}

Ball *BallGraf::getEsf()
{
    return esf;
}
