#ifndef PLAYER_H
#define PLAYER_H

class Player
{
private:
    double px;
    double py;
    double ay;
    double h;
    double w;

public:
    Player(double px, double py, double h, double w);
    void setVal(double x, double y, double a, double n);
    void actualizar(double dt);
    double getPx();
    double getPy();
    double setPy(double y);
    double getH();
    double getW();
    ~Player();
};

#endif // PLAYER_H
