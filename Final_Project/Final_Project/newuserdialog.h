#ifndef NEWUSERDIALOG_H
#define NEWUSERDIALOG_H

#include <QDialog>
#include "logindialog.h"


namespace Ui {
class NewUserDialog;
}

class NewUserDialog : public QDialog
{
    Q_OBJECT

public:
    explicit NewUserDialog(QWidget *parent = nullptr);
    ~NewUserDialog();
    void getData(QString username, QString password);
    QString getUserName();
    QString getPassword();
    bool getFlag();
    void updateUserlist();

private slots:
    void on_btnRegresar_3_clicked();
    void on_pushButton_clicked();

private:
    Ui::NewUserDialog *ui;
    QString usern;
    QString pass;
    bool flag = false;
    LoginDialog *login;
    QList<Usuario*> local;
};

#endif // NEWUSERDIALOG_H
