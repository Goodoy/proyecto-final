#include "validationdialog.h"
#include "ui_validationdialog.h"

ValidationDialog::ValidationDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ValidationDialog)
{
    ui->setupUi(this);

    generarNumerosAleatorios();

}

ValidationDialog::~ValidationDialog()
{
    delete ui;
}

void ValidationDialog::generarNumerosAleatorios(){
    QTime time = QTime::currentTime();
    qsrand((uint)time.msec());

    randNum1 = qrand() % 101;
    randNum2 = qrand() % 101;

    ui->num1->setText(QString::number(randNum1));
    ui->num2->setText(QString::number(randNum2));


}

void ValidationDialog::on_btnValidar_clicked()
{
    QString realResult = QString::number(randNum1+randNum2);
    QString inputResult = ui->result->text();

    if(realResult==inputResult){
        this->hide();
        valuser = new validationuserDialog();
        valuser->setModal(true);
        valuser->setWindowFlags(Qt::Window | Qt::FramelessWindowHint);
        valuser->exec();

    }else{
        QMessageBox::warning(this,"Validación incorrecta","La respuesta que ha ingresado es es incorrecta");
        ui->result->setText("");
        generarNumerosAleatorios();
    }
}
