#ifndef GOAL_H
#define GOAL_H
#include <QGraphicsItem>

class Goal{
private:
    double px;
    double py;
    double h;
    double b;
public:
    Goal(double px, double py, double b, double h);
    ~Goal();
    double getPx();
    double getPy();
    double getH();
    double getW();
};

#endif // GOAL_H
