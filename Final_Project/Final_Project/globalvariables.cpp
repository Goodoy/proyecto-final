#include "globalvariables.h"

GlobalVariables::GlobalVariables(QList<Usuario*> listUser)
{
    this->listUser = listUser;

}

QList<Usuario *> GlobalVariables::getListUser()
{
    return this->listUser;
}

void GlobalVariables::setListUser(QList<Usuario *> listUser)
{
    this->listUser = listUser;
}
