#ifndef VALIDATIONUSERDIALOG_H
#define VALIDATIONUSERDIALOG_H

#include <QDialog>
#include <QDialog>
#include "mainwindow.h"
#include <QTime>
#include <QMessageBox>
#include "logindialog.h"
#include "newuserdialog.h"

namespace Ui {
class validationuserDialog;
}

class validationuserDialog : public QDialog
{
    Q_OBJECT

public:
    explicit validationuserDialog(QWidget *parent = nullptr);
    ~validationuserDialog();

private slots:
    void on_pushButton_2_clicked();

    void on_btnRegresar_4_clicked();

    void on_pushButton_clicked();

private:
    Ui::validationuserDialog *ui;
    LoginDialog *login;
    NewUserDialog *newuser;

};

#endif // VALIDATIONUSERDIALOG_H
