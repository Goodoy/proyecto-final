#include "goalgraf.h"
#include <string.h>
#include <QPainter>
using namespace std;


GoalGraf1::GoalGraf1(double x, double y, double h, double w) : escala(1)
{
    goal1 = new Goal(x,y,h,w);
}

GoalGraf1::~GoalGraf1()
{
    delete goal1;
}

QRectF GoalGraf1::boundingRect() const
{
        return QRectF(-1*escala*goal1->getH(),-1*escala*goal1->getW(),2*escala*goal1->getH(),2*escala*goal1->getW());
}

void GoalGraf1::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
//    painter->setBrush(Qt::black);
//    painter->drawEllipse(boundingRect());    //dibuja una elipse encerrada en un boundingRect*/
    QPixmap pixmap;
    pixmap.load(":/Portería Left.png");
    painter->drawPixmap(boundingRect(), pixmap, pixmap.rect());

}

void GoalGraf1::setEscala(double s)
{
    escala = s;
}





GoalGraf2::GoalGraf2(double x, double y, double h, double w) : escala(1)
{
    goal2 = new Goal(x,y,h,w);
}

GoalGraf2::~GoalGraf2()
{
    delete goal2;
}

QRectF GoalGraf2::boundingRect() const
{
        return QRectF(-1*escala*goal2->getH(),-1*escala*goal2->getW(),2*escala*goal2->getH(),2*escala*goal2->getW());
}

void GoalGraf2::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
//    painter->setBrush(Qt::black);
//    painter->drawEllipse(boundingRect());    //dibuja una elipse encerrada en un boundingRect*/
    QPixmap pixmap;
    pixmap.load(":/Portería Right.png");
    painter->drawPixmap(boundingRect(), pixmap, pixmap.rect());

}

void GoalGraf2::setEscala(double s)
{
    escala = s;
}


